package ru.transaction.app.backend.service;

import io.smallrye.mutiny.Uni;
import ru.transaction.app.backend.model.TransactionPaymentDto;

public interface TransactionPaymentService {

    Uni<TransactionPaymentDto> commit(TransactionPaymentDto dto);

}
