package ru.transaction.app.backend.service.impl;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import ru.transaction.app.backend.kafka.TransactionPaymentProducer;
import ru.transaction.app.backend.model.TransactionPaymentDto;
import ru.transaction.app.backend.service.TransactionPaymentService;

import java.time.OffsetDateTime;
import java.util.UUID;

@ApplicationScoped
public class TransactionPaymentServiceImpl implements TransactionPaymentService {

    @Inject
    protected TransactionPaymentProducer producer;

    @Override
    public Uni<TransactionPaymentDto> commit(TransactionPaymentDto dto) {
        return Uni.createFrom()
                .item(dto)
                .map((el) -> {
                    el.setId(UUID.randomUUID().toString());
                    el.setCreatedTimestamp(OffsetDateTime.now());
                    return el;
                })
                .onItem()
                .invoke((el) -> producer.generate(el));
    }


}
