package ru.transaction.app.backend.controller;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.sse.OutboundSseEvent;
import jakarta.ws.rs.sse.Sse;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.jboss.resteasy.reactive.RestStreamElementType;
import ru.transaction.app.backend.model.TransactionPaymentDto;
import ru.transaction.app.backend.model.TransactionState;
import ru.transaction.app.backend.model.TransactionStateDto;
import ru.transaction.app.backend.service.TransactionPaymentService;

import java.util.Objects;

@Path("/api/v1/backend-service/transaction")
@ApplicationScoped
public class TransactionPaymentController {

    @Inject
    protected TransactionPaymentService service;

    @Inject
    @Channel("transaction-fail-end")
    protected Multi<TransactionPaymentDto> transactionsFail;

    @Inject
    @Channel("transaction-success-end")
    protected Multi<TransactionPaymentDto> transactionsSuccess;


    @Inject
    protected Sse sse;

    @POST
    public Uni<TransactionPaymentDto> commitTransaction(TransactionPaymentDto dto) {
        return service.commit(dto);
    }


    @GET
    @Path("/{userId}")
    @RestStreamElementType(MediaType.APPLICATION_JSON)
    public Multi<OutboundSseEvent> stream(@PathParam("userId") String userId) {
        return Multi.createBy().merging().streams(transactionsFail
                                .filter(Objects::nonNull)
                                .filter((el) -> Objects.equals(el.getAuthorId(), userId))
                                .map((el) -> {
                                    TransactionStateDto transactionStateDto = new TransactionStateDto();
                                    transactionStateDto.setTransactionId(el.getId());
                                    transactionStateDto.setAuthorId(el.getAuthorId());
                                    transactionStateDto.setState(TransactionState.FAIL);
                                    return transactionStateDto;
                                }),
                        transactionsSuccess.filter(Objects::nonNull)
                                .filter((el) -> Objects.equals(el.getAuthorId(), userId))
                                .map((el) -> {
                                    TransactionStateDto transactionStateDto = new TransactionStateDto();
                                    transactionStateDto.setTransactionId(el.getId());
                                    transactionStateDto.setAuthorId(el.getAuthorId());
                                    transactionStateDto.setState(TransactionState.SUCCESS);
                                    return transactionStateDto;
                                }))
                .map(transaction -> sse.newEventBuilder()
                        .name("transaction")
                        .data(transaction)
                        .build());
    }

}
