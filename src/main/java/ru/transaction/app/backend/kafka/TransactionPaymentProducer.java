package ru.transaction.app.backend.kafka;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import ru.transaction.app.backend.model.TransactionPaymentDto;

@ApplicationScoped
public class TransactionPaymentProducer {

    @Inject
    @Channel("transaction-input")
    protected Emitter<TransactionPaymentDto> transactionEmitter;


    public void generate(TransactionPaymentDto dto) {
        transactionEmitter.send(dto);
    }

}
